proc selectionSort(list:seq[int]):seq[int] =
    result = list

    proc indexOfMin(list:seq[int], startIndex:int): int =
        result = startIndex
        for i in startIndex .. list.high:
            if list[i] < list[result]:
                result = i

    for i in result.low .. result.high:
        swap(result[i], result[result.indexOfMin(i)])

when isMainModule:
    let
        seq1 = @[1, 3, 2, 56, 2, 1, 32, 45, 23, 41, 6, 47, 212, 32, 6, 98, 54, 457, 37, 4]
    echo seq1.selectionSort
