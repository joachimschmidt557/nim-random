proc gnomeSort(list:var seq[int]) = 
    var
        i = 0
    while i < list.high:
        if list[i] > list[i + 1]:
            swap(list[i + 1], list[i])
            if i > 0:
                dec i
        else:
            inc i

when isMainModule:
    var
        seq1 = @[1, 3, 2, 56, 2, 1, 32, 45, 23, 41, 6, 47, 212, 32, 6, 98, 54, 457, 37, 4]
    gnomeSort(seq1)
    echo seq1
