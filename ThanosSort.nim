proc thanosSort(list:seq[int]): seq[int] = 
    proc isSorted(list:seq[int]):bool =
        for i in list.low .. list.high - 1:
            if list[i] > list[i+1]:
                return false
        return true

    proc snap(list:seq[int]):seq[int] =
        for i in list.low .. (list.high div 2):
            result.add list[i * 2]

    result = list

    while not result.isSorted:
        result = result.snap

when isMainModule:
    let
        seq1 = @[1, 3, 5, 7, 23, 22, 13, 74, 4223, 3, 665, 62, 245, 2, 57, 3, 2, 4, 7, 4]
    echo seq1.thanosSort
